<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container" id="home-hero">
    <div class="row">
        <div class="col-12">
            <h1>We are a creative & strategic ecommerce agency.</h1>
            <p>Impress your audience with engaging content made for your online store.</p>
            <a class="secondary" href="<?php echo get_site_url(); ?>/ecommerce-consultation/" target="_blank">Grow my store</a>
        </div>
    </div>
</div>

<!-- Services -->
<div class="container" id="home-services">
    <div class="row">
        <h2 class="col-10">Best for brands who need content for & eyeballs on — their digital marketing.</h2>
    
        <!-- Content -->
        <div class="col-5">
            <a href="<?php echo get_site_url(); ?>/ecommerce-services/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/content.jpg"></a>
            <h3>Impress with content.</h3>
            <ul>
                <li>Advertising creatives</li>
                <li>Social media creatives</li>
                <li>Articles & blogs</li>
                <li>Emails & texts</li>
                <li>Product pages</li>
            </ul>
            <p>With so many competitors online, brands need great content to stand out more than ever.</p>
            <a href="<?php echo get_site_url(); ?>/ecommerce-services/" target="_blank">Learn more</a>
        </div>
    
        <!-- Communication -->
        <div class="col-5">
            <a href="<?php echo get_site_url(); ?>/ecommerce-services/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/communication.jpg"></a>
            <h3>Engage with it too.</h3>
            <ul>
                <li>Visual advertising</li>
                <li>Social media marketing</li>
                <li>SEO</li>
                <li>Google search ads</li>
                <li>Email marketing</li>
                <li>SMS marketing</li>
            </ul>
            <p>Does your audience know about your brand, and are they interacting with your content?</p>
            <a href="<?php echo get_site_url(); ?>/ecommerce-services/" target="_blank">Learn more</a>
        </div>
    </div>
</div>

<!-- CTA -->
<div class="container" id="home-cta">
    <div class="row">
        <div class="col-3">
            <a href="<?php echo get_site_url(); ?>/ecommerce-strategy/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/pdf-cover.png"></a>
        </div>
        <div class="col-7">
            <h2>Grow your online store with our guide.</h2>
            <p>Whether your brand is new or established, more revenue is always the name of the game. But how do you increase sales consistently over time? By focusing on the foundations of ecommerce and digital marketing. Forget the latest hacks and apps — get our guide!</p>
            <a class="secondary" href="<?php echo get_site_url(); ?>/ecommerce-strategy/" target="_blank">Grab PDF</a>
        </div>
    </div>
</div>

<!-- Blog Post -->
<div class="container" id="home-blog">
    <div class="row">
        <h2 class="col-10">Why choose ARWM for your ecommerce business.</h2>
        <div class="col-4">
            <a href="<?php echo get_site_url();?>/ecommerce-blog/why-choose-arwm/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/featured-blog.jpg"></a>
        </div>
        <div class="col-6">
            <span>Posted in October by Alexis Avellan</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit ipsum sed aliquet porta. Morbi nec nunc lectus. Aenean vitae lorem sed eros viverra laoreet. Phasellus ac purus tortor.</p>
            <a href="<?php echo get_site_url(); ?>/ecommerce-blog/why-choose-arwm/" target="_blank">Read more</a>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>