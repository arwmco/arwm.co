<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="blog-hero">
    <div class="row">
        <div class="col-10">
            <h1>We blog about all things ecommerce.</h1>
            <p>Stay informed with the latest news & tips in the ecommerce space.</p>
        </div>
    </div>
</div>

<!-- Latest Blog Post -->
<div class="container" id="blog-featured-post">
<?php

    $args = array('include' => array(53),);

    $blog_posts = get_posts( $args );

    if( ! empty( $blog_posts ) ){

        foreach ( $blog_posts as $p ){

            echo '<div class="row"><div class="col-10"><h2><a href="' . get_permalink($p->ID) . '" target="_blank">' . $p->post_title . '</h2></div><div class="col-4"><a href="' . get_permalink($p->ID) . '" target="_blank"></a></div><div class="col-6"><span>Featured post by ' . get_the_author_meta('display_name', $p->post_author) . '.</span><p>' . get_the_excerpt($p->ID) . '</p><a href="' . get_permalink($p->ID) . '" target="_blank">Read more</a></div></div>';

        }

    }

?>
</div>

<!-- CTA -->
<div class="container" id="blog-cta">
    <div class="row">
        <div class="col-10">
            <h2>Fuel your marketing with content & communication</h2>
        </div>
        <div class="col-4">
            <a href="<?php echo site_url(); ?>/ecommerce-consultation/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/rocket.svg"></a>
        </div>
        <div class="col-6">
            <p>2020 was the year that ecommerce exploded across the globe. More entrepreneurs have entered the market, and that means more competition for your brand. Stand out and stay on everyone's mind with our support.</p>
            <a class="secondary" href="<?php echo site_url(); ?>/ecommerce-consultation/" target="_blank">Grow my store</a>
        </div>
    </div>
</div>

<!-- Blog Posts -->
<div class="container" id="blog-posts">
<?php

    $args = array('exclude' => array(53),);

    $blog_posts = get_posts( $args );

    if( ! empty( $blog_posts ) ){

        foreach ( $blog_posts as $p ){

            echo '<div class="row"><div class="col-10"><h2><a href="' . get_permalink($p->ID) . '" target="_blank">' . $p->post_title . '</h2></div><div class="col-4"><a href="' . get_permalink($p->ID) . '" target="_blank"></a></div><div class="col-6"><span>Posted on ' . get_the_date() . ' by ' . get_the_author_meta('display_name', $p->post_author) . '.</span><p>' . get_the_excerpt($p->ID) . '</p><a href="' . get_permalink($p->ID) . '" target="_blank">Read more</a></div></div>';

        }

    }

?>
</div>

<!-- Footer -->
<?php get_footer(); ?>