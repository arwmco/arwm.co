<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="case-studies-hero">
    <div class="row">
        <div class="col-10">
            <h1>Our own ecommerce case studies.</h1>
            <p>We've grown brands, including our own. Learn how we did it.</p>
        </div>
    </div>
</div>

<!-- Case Studies -->
<div class="container" id="case-studies">
    <div class="row">
        <div class="col-10">
            <h2>Brands that have worked with us to grow their online stores.</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/freeze-pipe.jpg"></a>
        </div>
        <div class="col-6">
            <h3>Freeze Pipe</h3>
            <p>Existing in a unique product space, Freeze Pipe came to us when they needed help with their SEO after exhausting all relationships with thier social media influencers.</p>
            <a href="#">Read case study</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ecommerce-brand.jpg"></a>
        </div>
        <div class="col-6">
            <h3>Your ecommerce brand.</h3>
            <p>Join the other ecommerce businesses on this page with a case study of your own.</p>
            <a class="secondary" href="#">Grow my online store</a>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/tails-n-tummies.jpg"></a>
        </div>
        <div class="col-6">
            <h3>Tails 'N' Tummies</h3>
            <p>Our brand in the hyper-competitive pet supplement industry. Although it's a young business, we created this case study as a follow-along to showcase our expertise.</p>
            <a href="#">Read case study</a>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>