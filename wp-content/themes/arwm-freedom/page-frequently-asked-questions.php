<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="faq-hero">
    <div class="row">
        <div class="col-10">
            <h1>Questions we get asked frequently.</h1>
            <p>Got questions? Here is where you find answers.</p>
        </div>
    </div>
</div>

<!-- Questions & Answers -->
<div class="container" id="faq">
    <div class="row">
        <div class="col-10">
            <h2>General questions about our business and client relationships.</h2>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>How are your client relationships structured?</h3>
            </div>
            <p>We work with ecommerce brands through monthly retainers. We do take on one-time projects, but that depends on the client's goal.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>How are your monthly retainers priced?</h3>
            </div>
            <p>Prices vary depending on the services needed and the scope of work. Let's talk so we can get to know your brand and the challenges you're facing.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>Are the monthly retainers long-term?</h3>
            </div>
            <p>Our retainers are month-to-month, so you are free to stop using our services whenever you'd like. But we do offer better-priced longer-term retainers.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>Do you work with non-ecommerce businesses?</h3>
            </div>
            <p>We do not. Because of our experience and expertise in the space, we believe we provide the most value by working with ecommerce brands.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-10">
            <h2>Specific questions about our services.</h2>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>What kind of services does your agency offer?</h3>
            </div>
            <p>We divide our services into two categories, content and communication. Content includes creatives, articles and blogs, emails and texts, and product pages. Communication drives eyeballs to the content to increase sales.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>Can you help us with just digital marketing?</h3>
            </div>
            <p>Absolutely! You may have a team that already produces content for your brand, and you need help with marketing. We can do that, and we also create content for brands with a marketing team already.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>How much content do we get per month?</h3>
            </div>
            <p>It depends on what services your brand needs and the package you choose. After the first 30-minute introductory meeting, we put together a strategy for your business that includes all this information.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>Q</span>
                <h3>Can you help us sell our products on Amazon?</h3>
            </div>
            <p>At the moment, we do not offer Amazon seller management services. But we have a partner here in Orlando who does. Contact us using the form below, and we'll make an introduction.</p>
        </div>
    </div>
</div>

<!-- Contact Form -->
<div class="container" id="faq-contact-form">
    <div class="row">
        <div class="col-10">
            <h2>Still have a question not found here?</h2>
            <p>Fill out the form below, and we will get back to you within one business day.</p>
            <form>
            </form>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>