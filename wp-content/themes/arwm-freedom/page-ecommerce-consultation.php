<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="consultation-hero">
    <div class="row">
        <div class="col-10">
            <h1>Book your ecommerce consultation.</h1>
            <p>Growing your online store beings with getting to know each other.</p>
        </div>
    </div>
</div>

<!-- Steps -->
<div class="container" id="consultation-steps">
    <div class="row">
        <div class="col-10">
            <h2>Here is what to expect before you decide to work with us.</h2>
        </div>
        <div class="col-3">
            <h3>Step <span>1</span></h3>
            <p>You pick a date and time for us to have a 30-minute Zoom meeting. We use this as an opportunity to get to know you and your brand and the challenges you're facing.</p>
        </div>
        <div class="col-3">
            <h3>Step <span>2</span></h3>
            <p>After the introductory meeting, we will audit your online store and its marketing to design strategies to increase your online sales and revenue.</p>
        </div>
        <div class="col-3">
            <h3>Step <span>3</span></h3>
            <p>We'll meet once more to present and review our plan and answer questions you may have. After our discussion, decided if ARWM will be your ecommerce partner.</p>
        </div>
    </div>
</div>

<!-- CTA -->
<div class="container" id="consultation-cta">
    <div class="row">
        <div class="col-3">
        </div>
        <div class="col-7">
            <h2>Let's talk about your ecommerce store</h2>
            <p>Impressing and engaging your audience to grow your online store begins with a 30-minute Zoom meeting. Click on the button below to view our calendar and select a convenient date and time to meet with us.</p>
            <a class="secondary" href="<?php echo site_url(); ?>/ecommerce-consultation/">Schedule</a>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>