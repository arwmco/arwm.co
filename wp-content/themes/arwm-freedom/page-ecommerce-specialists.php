<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="about-hero">
    <div class="row">
        <div class="col-10">
            <h1>We're ecommerce specialists.</h1>
            <p>Get to know us, our values, and how ARWM began.</p>
        </div>
    </div>
</div>

<!-- Company -->
<div class="container" id="about-company">
    <div class="row">
        <div class="col-10">
            <h2>We're passionate about ecommerce & we're brand owners too.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/company.jpg">
        </div>
        <div class="col-6">
            <p>Agencies are a dime a dozen — they're everywhere. There are some good ones but more bad ones too, and only a few focus exclusively on ecommerce. The ones that do, not many of them have brands of their own. And that is where ARWM stands apart. We've been in the space for five years and have worked with multiple online stores, including our own. We're passionate about ecommerce and practicing what we preach.</p>
        </div>
    </div>
</div>

<!-- Values -->
<div class="container" id="about-values">
    <div class="row">
        <div class="col-10">
            <h2>The values that define why and how we work.</h2>
        </div>
        <div class="col-2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/partners.svg">
        </div>
        <div class="col-8">
            <h3>No accounts, only partners.</h3>
            <p>Unlike other agencies, our goal is not to sign up as many accounts as possible with the churn and burn mentality that has become so common in this industry. We believe that we do our best work when we're focused. And it's why we work with only a few brands at a time, choosing to be selective.</p>
        </div>
        <div class="col-2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/belief.svg">
        </div>
        <div class="col-8">
            <h3>Belief in your brand.</h3>
            <p>We have to believe that your ecommerce business has the potential to grow the same way that you do. We want to build long-term partnerships that produce value for everyone, your brand, our agency, and the customer.</p>
        </div>
        <div class="col-2">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/respect.svg">
        </div>
        <div class="col-8">
            <h3>Respect & communication.</h3>
            <p>We believe a prerequisite for successful relationships is respect from and for everyone involved. Running an online store is challenging, and we respect you for it. We help grow your brand, which is hard, and you respect us for it. Therefore, excellent and productive communication happens, resulting in success.</p>
        </div>
    </div>
</div>

<!-- Founder's Story -->
<div class="container" id="about-founder">
    <div class="row">
        <div class="col-6">
            <h2>Our founder's story in his own words.</h2>
            <p>Hello, my name is Alexis Gustav Avellan.</p>
            <p>Born and raised in New York City, I've been in marketing since 2010, when I started my career in online advertising for a large FX broker near Wall Street. As a designer, I would work on marketing campaigns that generated tens of thousands of trading account.</p>
            <p>In 2014, I made a career decision to expand my knowledge and skills by becoming a digital marketer. The switch led me to work for two well-known and well-respected agencies in New Jersey and New York. I gained a large amount of experience helping multiple businesses reach their goals.</p>
            <p>Eventually, I moved to Orlando, Florida, and continued my career in agencies. I found myself working more with startups and ecommerce brands. I ended up gravitating towards ecommerce because I found the volume of sales exciting. It led me to create two brands of my own that we're growing today.</p>
            <p>In October 2021, I launched ARWM. I wanted to agency that focused exclusively on ecommerce and applied lessions learned from our online stores. I wanted clients to have the confidence in knowing that we preacticed what we preached. But most of all, I wanted to do good work for and rub shoulders with other online store owners.</p>
            <a href="https://www.linkedin.com/in/alexisavellan/" target="_blank">Connect with me on LinkedIn</a>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>