<?php wp_footer(); ?>

<footer class="container">
    <div class="row">
        <div class="col-3">
            <ul>
                <li><a href="<?php echo get_site_url(); ?>">Home</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-specialists/">About</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-blog/">Blog</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-case-studies/">Case Studies</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-consultation/">Consultation</a></li>
                <li><a href="<?php echo get_site_url(); ?>/frequently-asked-questions/">FAQ</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-services/">Services</a></li>
                <li><a href="<?php echo get_site_url(); ?>/ecommerce-strategy/">Strategy</a></li>
            </ul>
        </div>

        <div class="col-4">
            <p>We are a creative and strategic partner for ecommerce brands. We know what it takes to grow online stores because we have several of our own.</p>
            <a href="#">Learn more</a>
        </div>

        <div class="col-3">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.svg">
        </div>
    </div>
</footer>

</body>

</html>