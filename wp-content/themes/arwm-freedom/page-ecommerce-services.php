<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="services-hero">
    <div class="row">
        <div class="col-10">
            <h1>Services for your ecommerce brand.</h1>
            <p>Increase sales by impression your audience and engaging with them.</p>
        </div>
    </div>
</div>

<!-- Service -> Creatives -->
<div class="container service" id="services-creatives">
    <div class="row">
        <div class="col-10">
            <h2>Creatives.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/creatives.jpg">
        </div>
        <div class="col-6">
            <p>Typically, the first time an ideal customer notices your brand or another is through a creative. From advertising to social media, they are everywhere. It's the first and only opportunity to make an impression that can lead to a sale. Make it count with great visuals, so audiences consider your brand and products.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Visual advertising to engage audiences.</h3>
            </div>
            <p>We creative eye catching ads to engage your audience & bring awareness to your brand. No matter the channel, you want to be sure to capture their attention & keep them captivated.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Social media marketing to engage followers.</h3>
            </div>
            <p>Social media marketing is where your audience is. They allow brands to talk to their audience while they get to know your brand. Promote your online store & build awareness on social platforms today.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
    </div>
</div>

<!-- Service -> Articles & blogs -->
<div class="container service" id="services-articles-blogs">
    <div class="row">
        <div class="col-10">
            <h2>Articles & blogs.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/articles-and-blogs.jpg">
        </div>
        <div class="col-6">
            <p>Online shoppers are savvier than ever, consuming information about and related to your products, researching before buying. Without articles and blogs on your store, they're getting that information elsewhere where there is little opportunity for your brand to captivate and educate. Keep them on your website and tell your brand's story.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>SEO to engage organic searches.</h3>
            </div>
            <p>Improve your organic traffic with SEO. This strategy allows you to grow the number of visitors going to your website. It focuses on optimizing your content to improve your rank on Google search.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Google Ads to engage paid searches.</h3>
            </div>
            <p>Get your name out there. Advertise on Google to bring more traffic to your website that will read your articles and blogs.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
    </div>
</div>

<!-- CTA -->
<div class="container" id="services-cta">
    <div class="row">
        <div class="col-10">
            <h2>Fuel your marketing with content & communication</h2>
        </div>
        <div class="col-4">
            <a href="<?php echo site_url(); ?>/ecommerce-consultation/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/rocket.svg"></a>
        </div>
        <div class="col-6">
            <p>2020 was the year that ecommerce exploded across the globe. More entrepreneurs have entered the market, and that means more competition for your brand. Stand out and stay on everyone's mind with our support.</p>
            <a class="secondary" href="<?php echo site_url(); ?>/ecommerce-consultation/" target="_blank">Grow my store</a>
        </div>
    </div>
</div>

<!-- Service -> Emails & texts -->
<div class="container service" id="services-emails-texts">
    <div class="row">
        <div class="col-10">
            <h2>Emails & texts.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/emails-and-texts.jpg">
        </div>
        <div class="col-6">
            <p>The most significant opportunity to increase sales and revenue lies in engaging with those who have interacted with your brand. From customers who have purchased to subscribers who have signed up, they all want to hear from you. Entertain them with the latest blog post or inform them of the newest sale, giving them reasons to return.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Email marketing to keep your lists engaged.</h3>
            </div>
            <p>Are you talking to your email subscribers? Email marketing keeps your subscriber list engaged & up to date with the latest happenings in your store. Increase sales with email flows & blasts today.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>SMS marketing to keep your subscribers engaged.</h3>
            </div>
            <p>Set up promotional campaigns through text. Reach your subscribers on their phone with SMS marketing. Talk about keeping your name on their minds & in their pockets.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
    </div>
</div>

<!-- Service -> Product pages -->
<div class="container service" id="services-product-pages">
    <div class="row">
        <div class="col-10">
            <h2>Product pages.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-pages.jpg">
        </div>
        <div class="col-6">
            <p>An essential but often overlooked piece of an online marketing campaign is the product page. When visitors land on it, it should grab their attention, answer their questions, position the product as a solution to their problems, and remove their objections. Turn your audience into customers with a great product page.</p>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Conversion rate optimization to increase your sales.</h3>
            </div>
            <p>Improve the number of sales for your online store with conversion rate optimization. You've got the visitors, now let's work together to increase the amount of sales you receive.</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
        <div class="col-5">
            <div class="two-col-header">
                <span>&</span>
                <h3>Average order value optimization to increase your sales.</h3>
            </div>
            <p>Average order value optimization gives you the most bang for your buck! It allows you to increase the amount of money spent in your online store by each customer. The more they spend the better it is for you!</p>
            <a href="#" target="_blank">Learn more</a>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>