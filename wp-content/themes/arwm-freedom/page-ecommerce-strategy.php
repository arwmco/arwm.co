<!-- Header -->
<?php get_header(); ?>

<!-- Hero -->
<div class="container page-hero" id="strategy-hero">
    <div class="row">
        <div class="col-10">
            <h1>An ecommerce strategy for brands of all sizes.</h1>
            <p>Timeless tactics to grow your online store.</p>
        </div>
    </div>
</div>

<!-- Content -->
<div class="container" id="strategy-content">
    <div class="row">
        <div class="col-10">
            <h2>Online store owners, are your sales not growing fast enough?</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ecommerce-sales.jpg">
        </div>
        <div class="col-6">
            <p>When you first launched, you had momentum behind you. Maybe Good Morning America mentioned your product on their show. Perhaps Facebook advertising crushed it for your brand and drove nearly all your sales. Your followers and subscribers could have possibly kept buying from you.</p>

            <p>But at some point, you noticed sales stopped increasing steadily. The momentum your brand once had ceased. Mentions in the media stopped, and people stopped responding to your Facebook ads like they once did. Your audience became saturated and slowly purchased less.</p>

            <p>No matter the cause, your online store generates the same amount of money, and it may even be declining. What's going to help increase revenue again?</p>
        </div>
    </div>
    <div class="row">
        <div class="col-10">
            <h2>With so many things to work on, what will truly move the needle?</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packaging.jpg">
        </div>
        <div class="col-6">
            <p>Revenue is what your online store needs to sell product inventory and fund more purchase orders, and it's a cycle of growth and investment into your business.</p>

            <p>There is no short supply of marketing activities you can perform for your ecommerce brand. From advertising on the latest trending social media network to integrating a "buy now, pay later" app in your checkout. Although these are things you should be doing, the question becomes, are they enough to drive growth?</p>
        </div>
    </div>
    <div class="row">
        <div class="col-10">
            <h2>Success exists where ecommerce meets all that digital marketing offers.</h2>
        </div>
        <div class="col-4">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ecommerce-digital-marketing.jpg">
        </div>
        <div class="col-6">
            <p>Running an online store and growing it are both challenging. But like many things, we can overcomplicate it. We look to the latest hacks and apps that promise to 4x our revenue. Ultimately, we're disappointed when they don't deliver, and we realize we wasted our time.</p>

            <p>But what is going to grow your brand is focusing on the foundations over and over again. Making sure you're driving awareness across marketing channels with great contnet, making the most out of store visitors and getting them to buy with an optimized site, providing value and getting it in return from customers and followers.</p>

            <p>The basics are often overlooked because they seem too obvious or are not sexy enough. But progress, and success, are to be had by making measurable improvements over time.</p>

            <p>Our guide will provide you with a strategy that tells you everything you should be doing as an online store owner with all that digital marketing offers.</p>
        </div>
    </div>
</div>

<!-- CTA -->
<div class="container" id="strategy-cta">
    <div class="row">
        <div class="col-3">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pdf-cover.png">
        </div>
        <div class="col-7">
            <h2>Strap a rocket to your brand with our guide.</h2>
            <p>Enter your name and email address below to have our strategy delivered straight to your inbox in a downloadable PDF.</p>

            <!-- Begin Mailchimp Signup Form -->
            <div id="mc_embed_signup">
            <form action="https://arwm.us5.list-manage.com/subscribe/post?u=27a47382418ee6d867da1f1d0&amp;id=2a4e7d13f6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
                <label for="mce-FNAME">First Name </label>
                <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
                <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
            </label>
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_27a47382418ee6d867da1f1d0_2a4e7d13f6" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
            </div>
            <!-- End MailChimp Signup Form -->
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>